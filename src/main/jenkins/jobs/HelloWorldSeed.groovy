//very simple pipeline script
(0..10).each { jobNo ->
    pipelineJob("Generated Job No $jobNo") {
        description("Generated from seed")
        definition {
            cps {
                script("""
                    stage "hello"
                    echo "hello world from $jobNo"
                    """)
            }
        }
    }
}

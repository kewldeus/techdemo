/**
 * @param releaseBranch name of branch to be used for release builds (e.g. /release/1.0.0 or /hotfix/2.0.3)
 */

def repos = [
        'project1', 'project2'
]


repos.each { repoName ->
    //create snapshot job
    pipelineJob("${repoName}_SNAPSHOT") {
        description("Snapshot Build for '${repoName}'")
        definition {
            cpsScm {
                scm {
                    git {
                        branch("*/develop")
                        remote {
                            credentials("user_git")
                            url("https://bitbucket-int/scm/iapj/${repoName}.git")
                        }
                    }
                }
                scriptPath('Jenkinsfile')
            }
        }
        environmentVariables {
            env("ORG_GRADLE_PROJECT_snapshot", true)
        }
    }
    //create release job
    pipelineJob("${repoName}_RELEASE") {
        description("<h3>Release Build for <span style='color:red'>$repoName</span> <span style='color:red'>$releaseBranch</span></h3>")
        definition {
            cpsScm {
                scm {
                    git {
                        branch("$releaseBranch")
                        remote {
                            credentials("user_git")
                            url("https://bitbucket-int/scm/iapj/${repoName}.git")
                        }
                    }
                }
                scriptPath('Jenkinsfile')
            }
        }
        environmentVariables {
            env("ORG_GRADLE_PROJECT_snapshot", false)
        }
    }
}

//FIXME obey dependency hierarchy for build order
def buildAllScript(repos, jobPath, jobSuffix){
    return repos.collectMany{repo ->
        ["stage '$repo'","build job: '$jobPath/${repo}_$jobSuffix'"]
    }.join("\n")
}

//********************************************
//Pipeline for all Snapshots
//********************************************


pipelineJob("ALL_JavaProjects_SNAPSHOT") {
    description("Release Build for all JavaProjects")
    definition {
        cps {
            script(buildAllScript(repos, "JavaProjects", "SNAPSHOT"))
            sandbox(true)
        }
    }
}

//********************************************
//Pipeline for all Releases
//********************************************


pipelineJob("ALL_JavaProjects_RELEASE") {
    description("Release Build for all JavaProjects")
    definition {
        cps {
            script(buildAllScript(repos, "JavaProjects", "RELEASE"))
            sandbox(true)
        }
    }
}
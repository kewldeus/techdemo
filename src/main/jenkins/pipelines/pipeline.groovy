def initTools() {
    env.JAVA_HOME = tool 'jdk-1.8.0'
    def gradleHome = tool 'gradle-2.12'

    //linux style
    env.PATH = "${env.JAVA_HOME}/bin:${gradleHome}/bin:${env.PATH}"

    //maven settings file is used for repo credentials, need for artifact publishing
    //env["ORG_GRADLE_PROJECT_mavenSettings"] = env["MAVEN_SETTINGS_FILE"]
}

def gradle(String task, boolean wrapper = true, boolean daemon = false){
    //Gradle project properties were read from environment via ORG_GRADLE_PROJECT_prop=somevalue
    def gradle = wrapper ? "./gradlew" : "gradle"
    def daemonSwitch = daemon ? "--daemon" : "--no-daemon"
    sh "$gradle $daemonSwitch $task --stacktrace"
}


def flow() {
    initTools()

    stage "checkout"
    git url: "https://@bitbucket-int/scm/iapj/techdemo.git"

    stage "build"
    gradle "build"

    stage 'deploy-test'
    echo "deploying test"

    stage 'integration-test'
    echo "perform end2end tests (soap requests)"

    stage 'publish'
    archive "build/*.jar"

    stage 'gen-reports'
    echo "generate site"
}

return this;